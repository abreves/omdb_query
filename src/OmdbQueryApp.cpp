/*
 * OmdbQueryApp.cpp
 *
 *  Created on: 13 de ago de 2019
 *      Author: breves
 */

#include "OmdbQueryApp.h"
#include "OmdbQuery.h"
/**
 *
 */
OmdbQueryApp::OmdbQueryApp(){
   // TODO Auto-generated constructor stub
   m_optDesc = new boost::program_options::options_description( "Options" );
   // @formatter:off
   m_optDesc->add_options()
         ( "help,h",   "Print this messages" )
         ( "search,s", po::value<std::vector<std::string>>()->multitoken()->zero_tokens()->composing(), "Search media by title." )
         ( "type,t",   po::value<std::string>(),  "Return media type (movie, series, episode)." );
   // @formatter:on
} // OmdbQueryApp::OmdbQueryApp(){

/**
 *
 */
OmdbQueryApp::~OmdbQueryApp(){
   // TODO Auto-generated destructor stub
   if ( m_optDesc ){
      delete m_optDesc;
   } // if ( m_optDesc ){
} // OmdbQueryApp::~OmdbQueryApp(){

/**
 *
 */
int OmdbQueryApp::run( int argc, char *argv[] ){

   try{
      po::variables_map vm;
      try{
         po::store( po::parse_command_line( argc, argv, *m_optDesc ), vm ); // can throw

         /** --help option */
         if ( vm.count( "help" ) ){
            printUsage();
            return 0;
         } // if ( vm.count( "help" ) ){
         po::notify( vm ); // throws on error, so do after help in case
                           // there are any problems
      }
      catch ( po::error &e ){
         printError( e );
         return 1;
      }

      // TODO:
      parseCommandLine( vm );
      OmdbQuery omdbQuery;
      omdbQuery.queryMedia( m_sMediaTitle, m_sMediaType );
   }
   catch ( std::exception &e ){
      // @formatter:off
      std::cerr
         << "Unhandled Exception reached the top of main: "
         << e.what()
         << ", application will now exit" << std::endl;
      // @formatter:on
      return 2;
   }

   return 0;
} // int OmdbQueryApp::run( int argc, char *argv[] ){

/**
 *
 */
void OmdbQueryApp::parseCommandLine( po::variables_map &vm ){

   m_sMediaTitle = "";
   m_sMediaType  = "";
   std::string sProcTitle;
   if ( vm.count( "search" ) ){
      std::vector<std::string> vsRawTitle = vm["search"].as<std::vector<std::string>>();

      int i = 0;
      for( std::string &s : vsRawTitle ){
         if ( i > 0 ){
            sProcTitle = sProcTitle + " ";
         } // if ( i > 0 ){
         sProcTitle = sProcTitle + s;
         i++;
      } // for( std::string &s : vsRawTitle ){

      m_sMediaTitle = sProcTitle;
   } // if (vm.count("search")){

   if ( vm.count( "type" ) ){
      std::string sMediaType = vm["type"].as<std::string>();

      if ( sMediaType == "movie" ||
           sMediaType == "series" ||
           sMediaType == "episode" ){
         m_sMediaType = sMediaType;
      } // if ( sMediaType == "movie" || ...
      else{
         std::cerr << "Invalid type: " << sMediaType << ". Ignoring." << std::endl;
      }
   } // if ( vm.count( "type" ) ){
} // void OmdbQueryApp::parseCommandLine( po::variables_map &vm ){

/**
 *
 */
void OmdbQueryApp::printUsage(){
   // @formatter:off
   std::cout
      << "Command Line Arguments"
      << std::endl
      << *m_optDesc
      << std::endl;
   // @formatter:on
} // void OmdbQueryApp::printUsage(){

/**
 *
 */
void OmdbQueryApp::printError( po::error &e ){
   // @formatter:off
   std::cerr
      << "ERROR: "
      << e.what()
      << std::endl
      << std::endl;
   std::cerr
      << *m_optDesc
      << std::endl;
   // @formatter:on
} // void OmdbQueryApp::printError( po::error &e ){
