/*
 * OmdbQuery.cpp
 *
 *  Created on: 13 de ago de 2019
 *      Author: breves
 */

#include "OmdbQuery.h"

std::string OmdbQuery::s_sBuffer;

/**
 *
 */
OmdbQuery::OmdbQuery(){
   // TODO Auto-generated constructor stub
} // OmdbQuery::OmdbQuery(){

/**
 *
 */
OmdbQuery::~OmdbQuery(){
   // TODO Auto-generated destructor stub
} // OmdbQuery::~OmdbQuery(){

/**
 *
 */
bool OmdbQuery::queryMedia( std::string sMediaTitle, std::string sMediaType ){

   std::string sProcTitle;
   int i = 0;
   for( char c : sMediaTitle ){
      if ( c == ' ' ){
         if ( i == 0 ){
            sProcTitle = sProcTitle + "%2f";
         } // if ( i == 0 ){
         i++;
         continue;
      } // if ( i > 0 ){
      sProcTitle = sProcTitle + c;
      i = 0;
   } // for( std::string &s : vsRawTitle ){

   OmdbQuery::s_sBuffer.clear();

   CURL *curl = nullptr;
   CURLcode codeRequestResult;

   // @formatter:off
   std::string sURL = "http://www.omdbapi.com/?apikey=1ada2c25&t=" + sProcTitle
                                                             + "&plot=short";

   if ( sMediaType != "" ){
      sURL = sURL + "&type=" + sMediaType;
   } // if ( sType != "" ){

   std::cout
      << "Processing Command Line Arguments: "
      << sURL
      << std::endl;

   curl = curl_easy_init();
   if ( !curl ){
      // @formatter:off
      std::cerr
         << "curl_easy_init() error. "
         << std::endl;
      // @formatter:on
      return false;
   } // if ( !curl ){

   // set curl target url
   curl_easy_setopt( curl, CURLOPT_URL, sURL.c_str() );

   /* set curl follow location */
   curl_easy_setopt( curl, CURLOPT_FOLLOWLOCATION, 1L );

   /* set callback for writing received data */
   curl_easy_setopt( curl, CURLOPT_WRITEFUNCTION, writeCurlResponse );

   std::cout << "\n\n==========================================================\n";
   // @formatter:off
   std::cout << "SEARCHING: "
             << sMediaTitle
             << ( sMediaType != "" ? " / " + sMediaType : "" )
             << std::endl;
   // @formatter:on
   std::cout << "Please wait...\n" << std::endl;
   /* Perform the request, res will get the return code */
   codeRequestResult = curl_easy_perform( curl );
   if( codeRequestResult != CURLE_OK ){
      // @formatter:off
      std::cerr
         << "curl_easy_perform() error: "
         << curl_easy_strerror( codeRequestResult )
         << std::endl;
      // @formatter:on
      return false;
   } // if( res != CURLE_OK ){

   std::stringstream ss( OmdbQuery::s_sBuffer );
   boost::property_tree::ptree pt;
   boost::property_tree::read_json(ss, pt);

   printData( pt );

   curl_easy_cleanup( curl );

   return true;
} // bool OmdbQuery::queryMedia( std::string sQueryString ){

/**
 * print the received data
 */
void OmdbQuery::printData( boost::property_tree::ptree const& pt ){
    using boost::property_tree::ptree;
    ptree::const_iterator end = pt.end();
    for (ptree::const_iterator it = pt.begin(); it != end; ++it) {
        std::cout << it->first << ": " << it->second.get_value<std::string>() << std::endl;
        printData( it->second );
    } // for (ptree::const_iterator it = pt.begin(); it != end; ++it) {
} // void OmdbQuery::printData( boost::property_tree::ptree const& pt ){

/**
 * callback function for writing curl received data
 * ptr   : points to the delivered data;
 * nmemb : is the size of that data;
 * size  : is always 1.
 */
size_t writeCurlResponse( void *ptr, size_t size, size_t nmemb, void *userdata ){
   OmdbQuery::s_sBuffer.append( (char*) ptr, size * nmemb );
   return size * nmemb;
} // size_t writeCurlResponse( void *ptr, size_t size, size_t nmemb, void *userdata ){
