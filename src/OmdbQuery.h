/*
 * OmdbQuery.h
 *
 *  Created on: 13 de ago de 2019
 *      Author: breves
 */

#ifndef OMDBQUERY_H_
#define OMDBQUERY_H_

#include <iostream>
#include <string>

#include <curl/curl.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>


#include <iterator>

class OmdbQuery{
   friend size_t writeCurlResponse( void *ptr, size_t size, size_t nmemb, void *userdata );

   public:
      OmdbQuery();
      virtual ~OmdbQuery();

      bool queryMedia( std::string sMediaTitle, std::string sMediaType = "" );
   private:
      static std::string s_sBuffer;

      void printData( boost::property_tree::ptree const& pt );
}; // class OmdbQuery{

size_t writeCurlResponse( void *ptr, size_t size, size_t nmemb, void *userdata );

#endif /* OMDBQUERY_H_ */
