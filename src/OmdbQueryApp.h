/*
 * OmdbQueryApp.h
 *
 *  Created on: 13 de ago de 2019
 *      Author: breves
 */

#ifndef OMDBQUERYAPP_H_
#define OMDBQUERYAPP_H_

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <iostream>
//#include <cstdlib>
#include <string>

#include <curl/curl.h>

//#include <iterator>

/**
 *
 */
class OmdbQueryApp{
   public:
      OmdbQueryApp();
      virtual ~OmdbQueryApp();

      int run( int argc, char *argv[] );


   private:
      void printUsage();
      void printError( po::error& e );

      void parseCommandLine( po::variables_map &vm );

      boost::program_options::options_description *m_optDesc;

      std::string m_sMediaTitle;
      std::string m_sMediaType;

}; // class OmdbQueryApp{

#endif /* OMDBQUERYAPP_H_ */
