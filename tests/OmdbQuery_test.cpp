/**
 *
 */
#include "../src/OmdbQuery.cpp"
#include "../src/OmdbQueryApp.cpp"

#include <gtest/gtest.h>

//TEST(test_case_name, test_name)

/**
 *
 */
TEST( OmdbQueryTest, test_queryMedia ){
   OmdbQuery omdbQuery;

   EXPECT_TRUE( omdbQuery.queryMedia( "The 100" ) );
   EXPECT_TRUE( omdbQuery.queryMedia( "Avatar" ) );
   EXPECT_TRUE( omdbQuery.queryMedia( "Avatar", "series" ) );
} // TEST( OmdbQueryTest, test_queryMedia ){

/**
 *
 */
TEST( OmdbQueryAppTest, test_run ){

   OmdbQueryApp omdbQueryApp;

   char *argv0  = "app-name";
   char *argv1  = "--search";
   char *argv2  = "star treks";
   char *argv3  = "treks";
   char *argv_t0[] = { argv0, argv1, argv2, argv3, NULL };
   EXPECT_EQ( 0, omdbQueryApp.run( 4, argv_t0 ) );

   argv0  = "app-name";
   argv1  = "--search";
   argv2  = "star trek";
   argv3  = "-t";
   char *argv4  = "series";
   char *argv_t1[] = { argv0, argv1, argv2, argv3, argv4, NULL };
   EXPECT_EQ( 0, omdbQueryApp.run( 5, argv_t1 ) );

} // TEST( OmdbQueryAppTest, test_run ){

/**
 *
 */
int main( int argc, char **argv ){
    testing::InitGoogleTest( &argc, argv );
    return RUN_ALL_TESTS();
} // int main( int argc, char **argv ){
